import os
import argparse
import json
from cv_helper.helper import Image, cv2


class ProcessImage(Image):
    def __init__(self, path: str):
        super().__init__(path)

    def gray(self):
        self.cv_image = cv2.imread(self.origin, cv2.IMREAD_GRAYSCALE)

    def outline(self):
        self.gray()
        ret, thresh = cv2.threshold(self.cv_image, 127, 255, 0)
        self.contours, self.hierarchy = cv2.findContours(
            thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        img = cv2.cvtColor(self.cv_image, cv2.COLOR_GRAY2BGR)
        self.cv_image = cv2.drawContours(
            img, self.contours, -1, (0, 255, 0), 2)


report = {
    'transformations': {},
    'processed files': {},
    'errors': {}
}


def file_check(path):
    if os.path.isfile(path):
        return path
    else:
        report['errors']['FileNotFoundError'] = path
        raise FileNotFoundError(path)


def dir_check(path):
    if os.path.isdir(path):
        return path
    else:
        report['errors']['NotADirectoryError'] = path
        raise NotADirectoryError(path)


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--source', type=file_check, help='Input image')
    parser.add_argument('-d', '--destination',
                        type=dir_check, help='Output image')
    parser.add_argument('-r', '--report', type=dir_check,
                        help='Report file location')
    parser.add_argument('-g', '--grayscale', action='store_true',
                        help='Process the input image to grayscale')
    parser.add_argument('-o', '--outline', action='store_true',
                        help='Process the input image with edge detection')

    return parser.parse_args()


def main():
    parsed_args = parse_arguments()
    file_counter = 0
    transformation_counter = 0

    if parsed_args.source:
        file_counter += 1
        image = ProcessImage(parsed_args.source)
        image.name = os.path.basename(parsed_args.source)
        report['processed files'][file_counter] = parsed_args.source

    if parsed_args.grayscale:
        transformation_counter += 1
        image.gray()
        image.save(parsed_args.destination + '/out_gray_' + image.name)
        report['transformations'][transformation_counter] = 'grayscale'

    if parsed_args.outline:
        transformation_counter += 1
        image.outline()
        image.save(parsed_args.destination + '/out_edges_' + image.name)
        report['transformations'][transformation_counter] = 'outline'

    if parsed_args.report:
        if report['transformations']:
            with open(parsed_args.report + '/report.json', 'w') as f:
                json.dump(report, f)
            print('Report was saved at: ' + parsed_args.report + '/report.json')
        else:
            # We don't report if there were no transformations
            print('Nothing to report.')


if __name__ == '__main__':
    main()
